# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

if Rails.env.development?
  
  house = House.new
  house.id = 2
  house.set_storage_encryption_key_plain("fT_1m9A75y3wZXgAQuxs", FogSecurityManager::CLOUD_STORAGE_SECRET_TOKEN)
  house.name = "Wolf House"
  house.address = "Celtic Ville, Wolfstreet"
  house.landmark = "Near Wolfenstein Castle"
  house.description = "2 km from Dragon Bus Terminal"

  location = house.build_location
  location.latitude, location.longitude = 10.7606682, 78.8147379

  house.cloud_channel_key = "u4WLSEGRZpSSXRRMcSGQ"
  
  house.save!

  admin_user = AdminUser.new
  admin_user.id = 2
  admin_user.email = "anjalitp@gmail.com"
  admin_user.password = "qwerty123"
  admin_user.password_confirmation = "qwerty123"
  admin_user.name = "Anjali T P"
  admin_user.phone_number = "9995516145"
  admin_user.house_id = 2
  admin_user.save!


  house = House.new
  house.id = 1
  house.set_storage_encryption_key_plain("Cw_5iJ3pxVM4wZYRXA3p", FogSecurityManager::CLOUD_STORAGE_SECRET_TOKEN)

  house.name = "Dragon House"
  house.address = "Dragon Ville, Dragon Street"
  house.landmark = "Near Pheonix House"
  house.description = "2 km from Dragon Bus Terminal"

  location = house.build_location
  location.latitude, location.longitude = 10.7598267, 78.81599
  
  house.cloud_channel_key = "59JV1T89Tmwn1UM5KVv6"
  
  house.save!

  admin_user = AdminUser.new
  admin_user.id = 1
  admin_user.email = "arjunthedragon@gmail.com"
  admin_user.password = "qwerty123"
  admin_user.password_confirmation = "qwerty123"
  admin_user.name = "Arjun K P"
  admin_user.phone_number = "9072263675"
  admin_user.house_id = 1
  admin_user.save!


  InfraParameter.create(
    [
      {
        "id"=>2, 
        "name"=>"CO", 
        "unit"=>"ppm",
        "retrieve_type"=>"average"
      }, 
      {
        "id"=>3, 
        "name"=>"CO2", 
        "unit"=>"ppm",
        "retrieve_type"=>"average"
      }, 
      {
        "id"=>4, 
        "name"=>"MOX GS822", 
        "unit"=>"V",
        "retrieve_type"=>"average"
      }, 
      {
        "id"=>1, 
        "name"=>"Temperature", 
        "unit"=>"celcius",
        "retrieve_type"=>"average"
      }, 
      {
        "id"=>5, 
        "name"=>"MOX TGS880", 
        "unit"=>"V",
        "retrieve_type"=>"average"
      }, 
      {
        "id"=>6, 
        "name"=>"Smoke Voltage", 
        "unit"=>"V",
        "retrieve_type"=>"average"
      }, 
      {
        "id"=>7, 
        "name"=>"Ion Voltage", 
        "unit"=>"V",
        "retrieve_type"=>"average"
      }, 
      {
        "id"=>8, 
        "name"=>"Electric Consumption", 
        "unit"=>"kwh",
        "retrieve_type"=>"last_value"
      }
    ]
  )
end