class CreateInfraReadings < ActiveRecord::Migration[5.0]
  def change
    create_table :infra_readings do |t|
      t.references :house, index: true
      t.references :infra_parameter

      t.float :value

      t.timestamps
    end
  end
end
