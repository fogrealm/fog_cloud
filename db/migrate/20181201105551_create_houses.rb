class CreateHouses < ActiveRecord::Migration[5.0]
  def change
    create_table :houses do |t|
      t.string :encrypted_name
      t.text :encrypted_address
      t.string :encrypted_landmark
      t.text :encrypted_description

      t.string :encrypted_cloud_channel_key
      t.string :storage_encryption_key, unique: true

      t.timestamps
    end
  end
end
