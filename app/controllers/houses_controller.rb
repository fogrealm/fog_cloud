class HousesController < InheritedResources::Base
  respond_to :json 

  def sync_all_houses
    # => Following is a dirty approach ..
    # => Only doing this coz cloud dies on running delayed jobs ...    
    
    House.fetch_and_update_houses_from_fog_maintain
  end

  def analyze_performance
    time_delay_request_number = params["time_delay_request_number"].to_i
    time_delay_situation = params["time_delay_situation"].to_s

    $redis.set("time_delay_request_number_#{time_delay_request_number}_situation_#{time_delay_situation}",Time.now.to_i)
            
  end
end