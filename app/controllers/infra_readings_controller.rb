class InfraReadingsController < InheritedResources::Base
  respond_to :json

  before_action :validate_payloaded_message, only: [ :create ]

  def create
    puts "\n payload_data_hash received - #{@payload_data_hash}"
    infra_reading_hash = @payload_data_hash["infra_reading"]
    @infra_reading = InfraReading.new(infra_reading_hash) rescue nil

    if @infra_reading.nil?
      render json: {
        status: 422,
        error: "Some issue occured while saving the computed infra reading in cloud"
      } and return
    end

    create! do |format|
      if @infra_reading.errors.any?
        render json: {
          status: 422, 
          error: "Some issue occured while saving the computed infra reading in cloud"
        } and return
      else
        render json: {
          status: 204,
          message: "Computed Infra Reading saved in Cloud"
        } and return
      end
    end
  end  


  private

  def validate_payloaded_message

    @symmetric_key_info_hash = FogSecurityManager.cloud_to_icfn_retrieve_plain_data_hash_from_asymmetrically_encrypted_data(params[:asymmetrically_encrypted_symmetric_key_info])# rescue nil
    puts "\n\n symmetric_key_info_hash - #{@symmetric_key_info_hash}"
    @channel_encryption_key = JSON.parse(@symmetric_key_info_hash)["channel_encryption_key"]
    puts "\n\n channel_encryption_key - #{@channel_encryption_key} "
    @payload_data_hash = FogSecurityManager.payload_data_hash_from_symmetrically_encrypted_payload_data(params[:encrypted_payload], @channel_encryption_key, nil, nil) rescue nil


    if @payload_data_hash.nil?
      raise CanCan::AccessDenied
    end

    received_message_digest = params[:message_digest]
    
    if @payload_data_hash.present?
      computed_message_digest = OpenSSL::HMAC.hexdigest("SHA256", @channel_encryption_key, @payload_data_hash.to_json)
    
      if computed_message_digest != received_message_digest
        raise CanCan::AccessDenied
      end
    end
  end


  def permitted_params
    params.permit(infra_reading: [:id, :house_id, :infra_parameter_id, :value])
  end

end
