class InfraParametersController < InheritedResources::Base
  respond_to :json 

  def sync_all_infra_parameters
    # => Following is a dirty approach ..
    # => Only doing this coz cloud dies on running delayed jobs ...    
    
    House.fetch_and_update_houses_from_fog_maintain
  end
end