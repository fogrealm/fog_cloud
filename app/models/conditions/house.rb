module Conditions::House

  def is_storage_encrypted?
    # => The functions returns whether the storage attributes are encrypted or not ..
    
    self.is_storage_encrypted == true
  end
end