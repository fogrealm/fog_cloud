class FogSecurityManager

  # => Constants ...
  DEFAULT_SYMMETRIC_ENCRYPTION_SCHEME = "AES-192-CBC"
  KEY_SIZE_BYTES = 24

  CLOUD_STORAGE_SECRET_TOKEN = "pupigxX9VBczkEzrLAA1"


  class << self

    def symmetrically_encrypted_payload_data(plain_payload_data_hash = {}, symmetric_encryption_scheme = nil)
      # => This function will symmetrically encrypt the payload data in hash format ...
      # => The hash is converted to json string ...
      # => The json string is encrypted and the Base64 encoded format is returned ...

      symmetric_encryption_scheme ||= DEFAULT_SYMMETRIC_ENCRYPTION_SCHEME
  
      cipher = OpenSSL::Cipher.new(symmetric_encryption_scheme)
      cipher.encrypt

      authentication_token = CloudMain::CLOUD_AUTHENTICATION_TOKEN
      authentication_token_padded_24_bytes = authentication_token.ljust(KEY_SIZE_BYTES, '0')
      cipher.key = authentication_token_padded_24_bytes

      plain_payload_data = plain_payload_data_hash.to_json

      encrypted_payload_data = cipher.update(plain_payload_data) + cipher.final
      Base64.encode64(encrypted_payload_data)    
    end


    def retrieve_plain_json_from_encrypted_json_list(encrypted_json_list = nil)
      # => This method will return the array of plain hash jsons from the encrypted json list ...

      return if encrypted_json_list.nil?

      plain_json_list = []
      encrypted_json_list.each do |encrypted_json|
        plain_json_list << retrieve_plain_json_from_encrypted_json(encrypted_json)
      end

      plain_json_list
    end
   
    
    def retrieve_plain_json_from_encrypted_json(encrypted_json)
      # => The following function retrieves the plain hash based data from the json response ...
      # => The json response includes asymmetrically encrypted symmetric key information and symmetrically encrypted payload data  ..
      # => The symmetric key and initial vector are retrieved from the asymmetrically encrypted data ...
      # => The icfn information from yml file is used to decrypt the asymmetrically encrypted content as the private key as the house information ...
      # => The symmetric key information retrieved is used to decrypt the encrypted data payload ...
      # => The net result is the payload data ...

      asymmetrically_encrypted_symmetric_encryption_info = Base64.decode64(encrypted_json["asymmetrically_encrypted_symmetric_encryption_info"])
      symmetrically_encrypted_payload_data = Base64.decode64(encrypted_json["symmetrically_encrypted_payload_data"])
      
      private_key_content = File.read("cloud_server_#{CloudMain::CLOUD_ID}_private_key.pem")
      private_key = OpenSSL::PKey::RSA.new(private_key_content, nil)
      asymmetrically_encrypted_symmetric_encryption_info_json_string = private_key.private_decrypt(Base64.decode64(asymmetrically_encrypted_symmetric_encryption_info))

      asymmetrically_encrypted_symmetric_encryption_info_json = JSON.parse(asymmetrically_encrypted_symmetric_encryption_info_json_string)
      base64_encoded_symmetric_key = asymmetrically_encrypted_symmetric_encryption_info_json["symmetric_key"]
      base64_encoded_initial_vector = asymmetrically_encrypted_symmetric_encryption_info_json["initial_vector"]

      symmetric_encryption_scheme = asymmetrically_encrypted_symmetric_encryption_info_json["symmetric_encryption_scheme"]
      symmetric_key = Base64.decode64(base64_encoded_symmetric_key)
      initial_vector = Base64.decode64(base64_encoded_initial_vector)

      puts "\n encryption_info - #{symmetric_encryption_scheme}, #{symmetric_key}, #{initial_vector}"


      decipher = OpenSSL::Cipher.new(symmetric_encryption_scheme)
      decipher.decrypt
      decipher.key = symmetric_key
      decipher.iv = initial_vector

      payload_data_json_string = decipher.update(symmetrically_encrypted_payload_data) + decipher.final
      plain_json = JSON.parse(payload_data_json_string)
      plain_json
    end


    def cloud_to_icfn_retrieve_plain_data_hash_from_asymmetrically_encrypted_data(encrypted_data)
      # => This methods asymmetrically decrypts the payload data send from icfn to cloud ...

      raise "Invalid encrypted_data " if encrypted_data.nil?

      base64_decoded_encrypted_data = Base64.decode64(encrypted_data)

      private_key_content = File.read("cloud_#{CloudMain::CLOUD_ID}_icfn_private_key.pem")
      private_key = OpenSSL::PKey::RSA.new(private_key_content, nil)
      plain_data_hash = private_key.private_decrypt(base64_decoded_encrypted_data)
      plain_data_hash
    end


    def payload_data_hash_from_symmetrically_encrypted_payload_data(encrypted_payload = nil, encryption_key = nil, encryption_scheme = nil, opts = {})
      # => The following function retrieves the plain hash based data from the json response ...
      # => The json response includes asymmetrically encrypted symmetric key information and symmetrically encrypted payload data  ..
      # => The symmetric key and initial vector are retrieved from the asymmetrically encrypted data ...
      # => The current house information is used to decrypt the asymmetrically encrypted content as the private key as the house information ...
      # => The symmetric key information retrieved is used to decrypt the encrypted data payload ...
      # => The net result is the payload data ...

      raise "Invalid encrypted payload data" if encrypted_payload.nil?
      raise "Invalid Encryption Key" if encryption_key.nil?

      encryption_scheme ||= DEFAULT_SYMMETRIC_ENCRYPTION_SCHEME
      base64_decoded_symmetrically_encrypted_payload = Base64.decode64(encrypted_payload)
      encryption_key_padded_keysize_bytes = encryption_key.ljust(KEY_SIZE_BYTES, '0')

      decipher = OpenSSL::Cipher.new(encryption_scheme)
      decipher.decrypt
      decipher.key = encryption_key_padded_keysize_bytes
    
      payload_data_json_string = decipher.update(base64_decoded_symmetrically_encrypted_payload) + decipher.final
      payload_data_hash = JSON.parse(payload_data_json_string)
      payload_data_hash
    end


    def encrypt_plain_string(plain_string = nil, encryption_key = nil, encryption_scheme = nil, opt = {})
      # => This is a simple method to encrypt a given string ...
      # => The arguments passed here are encryption key and encryption_scheme ...

      raise "Invalid plain string! Plain string cannot be nil or empty string" if plain_string.blank?
      raise "Invalid Encryption Key" if encryption_key.nil?

      encryption_scheme ||= DEFAULT_SYMMETRIC_ENCRYPTION_SCHEME

      cipher = OpenSSL::Cipher.new(encryption_scheme)
      cipher.encrypt

      encryption_key_padded_keysize_bytes = encryption_key.ljust(KEY_SIZE_BYTES, '0')
      cipher.key = encryption_key_padded_keysize_bytes

      encrypted_string = cipher.update(plain_string) + cipher.final
      Base64.encode64(encrypted_string)
    end


    def decrypt_encrypted_string(encrypted_string = nil, encryption_key = nil, encryption_scheme = nil, opt = {})
      # => This is a simple method to encrypt a given string ...
      # => The arguments passed here are encryption key and encryption_scheme ...

      raise "Invalid plain string! Plain string cannot be nil or empty string" if encrypted_string.blank?
      raise "Invalid Encryption Key" if encryption_key.nil?

      encryption_scheme ||= DEFAULT_SYMMETRIC_ENCRYPTION_SCHEME
      base64_decoded_symmetrically_encrypted_string = Base64.decode64(encrypted_string)
      encryption_key_padded_keysize_bytes = encryption_key.ljust(KEY_SIZE_BYTES, '0')

      decipher = OpenSSL::Cipher.new(encryption_scheme)
      decipher.decrypt
      decipher.key = encryption_key_padded_keysize_bytes
    
      plain_string = decipher.update(base64_decoded_symmetrically_encrypted_string) + decipher.final
      plain_string 
    end


    def fog_request_encoded_token_signature
      # => The following method generates the base64 token signature ...
      # => The message signed contains house_id and house_authentication_token ...
  
      private_key_content = File.read("cloud_server_#{CloudMain::CLOUD_ID}_private_key.pem")
      private_key = OpenSSL::PKey::RSA.new(private_key_content, nil)
      plain_message = { cloud_id: CloudMain::CLOUD_ID, cloud_authentication_token: CloudMain::CLOUD_AUTHENTICATION_TOKEN }.to_json            
      signature = private_key.sign(OpenSSL::Digest::SHA256.new, plain_message)
      Base64.encode64(signature)
    end
  end
end