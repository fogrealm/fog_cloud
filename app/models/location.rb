class Location < ApplicationRecord
  #include Formats::Location

  # => Relations ...
  belongs_to :locatable, polymorphic: true


  # => Validations ...
  validates :latitude, :longitude, presence: true


  # => Geocoding ...
  reverse_geocoded_by :latitude, :longitude


  # => Scopes ...
  scope :locatable_type_house, -> {
    where(
      locatable_type: House.to_s
    )
  }

end