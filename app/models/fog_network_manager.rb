class FogNetworkManager

  FOG_MAINTAIN_BASE_URL = "http://ec2-13-232-107-4.ap-south-1.compute.amazonaws.com"
  #FOG_MAINTAIN_BASE_URL = "http://0.0.0.0:5050"


  class << self


    def fetch_update_status
      # => This Performs an API call to fog maintain to check whether any update is required or not ...

      fog_api = FogApi.new(FOG_MAINTAIN_BASE_URL)
      check_update_route = "/cloud_servers/1/check_updates.json"
      
      response = fog_api.conn.get check_update_route
      puts "response - #{response.body}"

      response_body = JSON.parse(response.body)
      updation_status = response_body["updation_status"]

      status  = updation_status["is_recently_updated"]

      status
      #puts "\n updation status - #{updation_status}"
      
    end


    def synchronize_all_model_entities(fog_fetch_api = nil, fog_post_api = nil)
      # => This method will update all the model entities within the fog layer 1 ...
      # => The Data is fetched from fog maintain server ...
      # => Once the data within the layer 1 is synchronized ...
      # => Then, the corresponding layer 2 entities (if only needed to ) will be also synchronized ...

      InfraParameter.fetch_and_update_infra_parameters_from_fog_maintain(fog_fetch_api, fog_post_api)
      House.fetch_and_update_houses_from_fog_maintain(fog_fetch_api, fog_post_api)      
    end

    
    def update_cloud_server_updation_status_in_fog_maintain
      # => This method will update the status of the house as updated ...
    
      fog_updation_complete_api = FogApi.new(FogNetworkManager::FOG_MAINTAIN_BASE_URL)    
      set_icfn_node_update_route = "/cloud_servers/1/set_cloud_server_recently_updated.json"

      fog_updation_complete_api.conn.post set_icfn_node_update_route
    end    

    

    
  end

end