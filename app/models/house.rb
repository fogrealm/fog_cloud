class House < ApplicationRecord
  include ActiveRecordBaseCommon::Validations
  include Attributes::House
  include Actions::House
  include Conditions::House
  include Formats::House

  # => Relations ...
  has_many :admin_users, :dependent => :destroy
  has_many :infra_readings, :dependent => :destroy
  has_many :infra_parameters, :through => :infra_readings

  has_one :location, as: :locatable, :dependent => :destroy
  

  # => Validations ...
  validates :encrypted_name, :encrypted_address, :encrypted_landmark, presence: true
  validates :location, has_one: true
  

  # => Associations ...
  accepts_nested_attributes_for :location


  # => Callbacks ...
  before_validation on: :create do
    # => prefill attributes ...

    if self.storage_encryption_key.blank?
      self.storage_encryption_key = self.generate_new_encrypted_storage_encryption_key
    end
  end
end
