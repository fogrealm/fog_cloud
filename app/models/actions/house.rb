module Actions::House
  
  def self.included(receiver)
    receiver.extend         ClassMethods
    receiver.send :include, InstanceMethods
  end


  module InstanceMethods

    def reset_storage_encryption_key
      # => This method reset the authentication token to a new one ...

      self.storage_encryption_key = generate_new_encryted_storage_encryption_key
      self.save!
    end

  
    def generate_new_encryted_storage_encryption_key
      # => This method generate a new authentication token ...
    
      key = Devise.friendly_token
      encrypted_key = FogSecurityManager.encrypt_plain_string(key, FogSecurityManager::CLOUD_STORAGE_SECRET_TOKEN)

      while House.where(storage_encryption_key: encrypted_key).count > 0
        key = Devise.friendly_token
        encrypted_key = FogSecurityManager.encrypt_plain_string(key, FogSecurityManager::CLOUD_STORAGE_SECRET_TOKEN)
      end
      
      encrypted_key
    end

=begin
    def set_storage_attributes_encrypted
      # => This will the encrypted the storage attributes ...
      
      return nil unless self.is_storage_encrypted?

      house_storage_encryption_key = self.storage_encryption_key_plain(FogSecurityManager::CLOUD_STORAGE_SECRET_TOKEN)

      return nil if house_storage_encryption_key.nil?

      self.set_name(self.name, house_storage_encryption_key)
      self.set_address(self.address, house_storage_encryption_key)
      self.set_landmark(self.landmark, house_storage_encryption_key)
      self.set_description(self.description, house_storage_encryption_key)
      self.set_cloud_channel_key(self.cloud_channel_key, house_storage_encryption_key)
    
      self.save!
    end


    def set_name(string_value = nil, house_storage_encryption_key = nil)
      # => This function checks whether the storage encryption mode is set or not ...
      # => If it is set, it set the encrypted value or the plain value ...

      return nil if string_value.nil?

      self.name = self.if_storage_encrypted ? FogSecurityManager.encrypt_plain_string(string_value, house_storage_encryption_key, nil, nil) : string_value
    end


    def set_address(string_value = nil, house_storage_encryption_key = nil)
      # => This function checks whether the storage encryption mode is set or not ...
      # => If it is set, it set the encrypted value or the plain value ...

      return nil if string_value.nil?

      self.address = self.if_storage_encrypted ? FogSecurityManager.encrypt_plain_string(string_value, house_storage_encryption_key, nil, nil) : string_value
    end


    def set_landmark(string_value = nil, house_storage_encryption_key = nil)
      # => This function checks whether the storage encryption mode is set or not ...
      # => If it is set, it set the encrypted value or the plain value ...

      return nil if string_value.nil?

      self.landmark = self.if_storage_encrypted ? FogSecurityManager.encrypt_plain_string(string_value, house_storage_encryption_key, nil, nil) : string_value
    end


    def set_description(string_value = nil, house_storage_encryption_key = nil)
      # => This function checks whether the storage encryption mode is set or not ...
      # => If it is set, it set the encrypted value or the plain value ...

      return nil if string_value.nil?

      self.description = self.if_storage_encrypted ? FogSecurityManager.encrypt_plain_string(string_value, house_storage_encryption_key, nil, nil) : string_value
    end


    def set_cloud_channel_key(string_value = nil, house_storage_encryption_key = nil)
      # => This function checks whether the storage encryption mode is set or not ...
      # => If it is set, it set the encrypted value or the plain value ...

      return nil if string_value.nil?

      self.cloud_channel_key = self.if_storage_encrypted ? FogSecurityManager.encrypt_plain_string(string_value, house_storage_encryption_key, nil, nil) : string_value
    end
=end  
  end


  module ClassMethods
    

    def fetch_and_update_houses_from_fog_maintain( fog_fetch_api = nil, fog_post_api = nil )
    # => This method to fetch the current house from server and update this in the fog ...

      fog_fetch_api = FogApi.new(FogNetworkManager::FOG_MAINTAIN_BASE_URL) if fog_fetch_api.nil?
      
      fetch_houses_route = "/houses.json"

      encoded_token_signature = FogSecurityManager.fog_request_encoded_token_signature
      plain_payload_data_hash = {
        encoded_token_signature: encoded_token_signature,
        cloud_id: CloudMain::CLOUD_ID,
        cloud_authentication_token: CloudMain::CLOUD_AUTHENTICATION_TOKEN
      }
      encrypted_request_payload = FogSecurityManager.symmetrically_encrypted_payload_data(plain_payload_data_hash, nil)

      encrypted_json_list_response = fog_fetch_api.conn.get fetch_houses_route, { encrypted_request_payload: encrypted_request_payload, layer: 4, cloud_id: CloudMain::CLOUD_ID }
      puts "encrypted json list response - #{encrypted_json_list_response.body}"

      encrypted_json_list = JSON.parse(encrypted_json_list_response.body)
      houses_json = FogSecurityManager.retrieve_plain_json_from_encrypted_json_list(encrypted_json_list) rescue nil

      puts "\n plain response json - #{houses_json}"

      if houses_json.present?
        current_existing_ids = houses_json.map{|house_json| house_json["id"]}
        houses_to_be_removed = House.where.not(id: current_existing_ids)
        houses_to_be_removed.destroy_all

        houses_json.each do |house_json|

          id = house_json["id"]
          name = house_json["name"]
          address = house_json["address"]
          landmark = house_json["description"]

          location_id = house_json["location"]["id"]
          location_latitude = house_json["location"]["latitude"]
          location_longitude = house_json["location"]["longitude"]

          cloud_channel_key = nil
          nearby_cloud_server_json = house_json["nearby_cloud_server"]
          if nearby_cloud_server_json.present?
            cloud_channel_key = house_json["nearby_cloud_server"]["cloud_channel_key"]
          end

          house = House.where(id: id).first

          if house.present?
            house.id = id
            house.name = name
            house.address = address
            house.landmark = landmark

            house.location.id = location_id
            house.location.latitude = location_latitude
            house.location.longitude = location_longitude

            house.cloud_channel_key = cloud_channel_key

            house.save!
          else
            house = House.new
            house.set_storage_encryption_key_plain(Device.friendly_token, FogSecurityManager::CLOUD_STORAGE_SECRET_TOKEN)

            house.id = id
            house.name = name
            house.address = address
            house.landmark = landmark

            location = house.build_location
            location.id = location_id
            location.latitude = location_latitude
            location.longitude = location_longitude

            house.cloud_channel_key = cloud_channel_key

            house.save!
          end
    
        end
    
      end
    end

    # => Commenting this cloud server dies on using delayed jobs ...
    # => But should be uncommented this later on ...
    #handle_asynchronously :fetch_and_update_houses_from_fog_maintain

  end
  
  module InstanceMethods
    
  end
  
  
end