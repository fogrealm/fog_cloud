class InfraParameter < ApplicationRecord
  include Attributes::InfraParameter
  include Actions::InfraParameter
  # include Formats::InfraParameter

  # => Constants ...
  INFRA_PARAMETER_RETRIEVE_TYPE_SUM = "sum"
  INFRA_PARAMETER_RETRIEVE_TYPE_AVERAGE = "average"
  INFRA_PARAMETER_RETRIEVE_TYPE_LAST_VALUE = "last_value"

  INFRA_PARAMETER_RETRIEVE_TYPES = [
    INFRA_PARAMETER_RETRIEVE_TYPE_SUM,
    INFRA_PARAMETER_RETRIEVE_TYPE_AVERAGE,
    INFRA_PARAMETER_RETRIEVE_TYPE_LAST_VALUE
  ]

  # => Relations ...
  has_many :infra_readings, dependent: :destroy
  has_many :houses, through: :infra_readings


  # => Validations ...
  validates :name, presence: true
  validates :unit, presence: true
  validates :retrieve_type, inclusion: { :in => INFRA_PARAMETER_RETRIEVE_TYPES }

  validates_uniqueness_of :name, scope: :unit


  before_validation do
  	# => set the retrieve type as average if nothing is mentioned ...

    if self.retrieve_type.nil?
      self.retrieve_type = INFRA_PARAMETER_RETRIEVE_TYPE_AVERAGE
    end
  end
  
end
