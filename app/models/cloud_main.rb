class CloudMain

  # => Constants ...
  CREDENTIALS_CONFIG = YAML.load_file("#{::Rails.root}/config/cloud_credentials.yml")[::Rails.env]
  CLOUD_ID = CREDENTIALS_CONFIG['cloud_id'].to_i
  CLOUD_AUTHENTICATION_TOKEN = CREDENTIALS_CONFIG['cloud_authentication_token']


  class << self

    # => Accessors ...
    attr_accessor :data_sync_scheduler # => This needs to be moved from here later. Not clean...


    # => Methods ...
    def initiate_icfn_services

      
      CloudMain.synchronize_data_periodically
      Rails.logger.info "\n Initiated Updated Management - Cloud"    
    end


    def synchronize_data_periodically
      # => Method which will check for data content based updates from the fog maintain server ..

      return if House.count < 1

      CloudMain.data_sync_scheduler = Rufus::Scheduler.new if CloudMain.data_sync_scheduler.nil?

      
      CloudMain.data_sync_scheduler.every '30s' do
      
        update_status = FogNetworkManager.fetch_update_status
    
        if update_status == false 
          # => Need to look for a appraoch later so that i can store the fog_fetch_api and fog_post_api in a variable ...
          # => Currently can't do since it is leading to a delay job deserialization problem ...

          FogNetworkManager.synchronize_all_model_entities(nil, nil)
          FogNetworkManager.update_cloud_server_updation_status_in_fog_maintain
        end
    
      end

    end

  end  
end