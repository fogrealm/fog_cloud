class AdminUser < ApplicationRecord
  
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, 
         :recoverable, :rememberable, :trackable, :validatable

  # => Relations ...
  belongs_to :house

  # => Validations ...
  validates :name, presence: true
  validates :phone_number, presence: true, format: { with: ActiveRecordBaseCommon::Validations::VALID_PHONE_FORMAT }
  
end
