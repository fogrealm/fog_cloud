module Formats::House

  def as_json( opts = {})
    {
      id: self.id,
      name: self.encrypted_name,
      address: self.encrypted_address,
      landmark: self.encrypted_landmark,
      description: self.encrypted_description
    }
  end

end