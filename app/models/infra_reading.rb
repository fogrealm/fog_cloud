class InfraReading < ApplicationRecord
  #include Formats::InfraReading

  # => Constants ...
  INFRA_READING_AREL = InfraReading.arel_table
  INFRA_READING_PERIOD = 24 * 3600

  # => Relations ...
  belongs_to :house
  belongs_to :infra_parameter


  # => Validations ...
  validates :value, presence: true
  validates :house, :infra_parameter, presence: true

  
  # => Delegations ...
  delegate :unit, to: :infra_parameter


  # => Scopes ...
  scope :created_between, ->(start_time, end_time) {
    where(
      INFRA_READING_AREL[:created_at]
      .gt(start_time)
    )
    .where(
      INFRA_READING_AREL[:created_at]
      .lt(end_time)
    )
  
  }


end
