module Attributes::House

  def set_storage_encryption_key_plain(storage_encryption_key_plain_format, global_storage_key = nil)
    # => This would set the plain form of the storage encryption key ...
    # => Automatically the encrypted form of the key will be set ...

    return nil if global_storage_key.nil?

    self.storage_encryption_key = FogSecurityManager.encrypt_plain_string(storage_encryption_key_plain_format, global_storage_key, nil, nil)# rescue nil
  end


  def storage_encryption_key_plain(global_storage_key = nil)
    # => This would return the plain form of the storage encryption key ...

    return nil if global_storage_key.nil?

    FogSecurityManager.decrypt_encrypted_string(self.storage_encryption_key, global_storage_key, nil, nil)# rescue nil
  end


  def name=(value_string)
    # => This function set the value to its encrypted form ...
    
    house_storage_encryption_key = self.storage_encryption_key_plain(FogSecurityManager::CLOUD_STORAGE_SECRET_TOKEN)
    return nil if house_storage_encryption_key.nil?

    self.encrypted_name = FogSecurityManager.encrypt_plain_string(value_string, house_storage_encryption_key, nil, nil) rescue nil
  end


  def address=(value_string)
    # => This function set the value to its encrypted form ...
    
    house_storage_encryption_key = self.storage_encryption_key_plain(FogSecurityManager::CLOUD_STORAGE_SECRET_TOKEN)
    return nil if house_storage_encryption_key.nil?

    self.encrypted_address = FogSecurityManager.encrypt_plain_string(value_string, house_storage_encryption_key, nil, nil) rescue nil
  end


  def landmark=(value_string)
    # => This function set the value to its encrypted form ...
    
    house_storage_encryption_key = self.storage_encryption_key_plain(FogSecurityManager::CLOUD_STORAGE_SECRET_TOKEN)
    return nil if house_storage_encryption_key.nil?

    self.encrypted_landmark = FogSecurityManager.encrypt_plain_string(value_string, house_storage_encryption_key, nil, nil) rescue nil
  end


  def description=(value_string)
    # => This function set the value to its encrypted form ...
    
    house_storage_encryption_key = self.storage_encryption_key_plain(FogSecurityManager::CLOUD_STORAGE_SECRET_TOKEN)
    return nil if house_storage_encryption_key.nil?

    self.encrypted_description = FogSecurityManager.encrypt_plain_string(value_string, house_storage_encryption_key, nil, nil) rescue nil
  end


  def cloud_channel_key=(value_string)
    # => This function set the value to its encrypted form ...
    
    house_storage_encryption_key = self.storage_encryption_key_plain(FogSecurityManager::CLOUD_STORAGE_SECRET_TOKEN)
    return nil if house_storage_encryption_key.nil?

    self.encrypted_cloud_channel_key = FogSecurityManager.encrypt_plain_string(value_string, house_storage_encryption_key, nil, nil) rescue nil
  end


  def name
    # => returns the decrypted plain version of the encrypted string stored in database ...

    house_storage_encryption_key = self.storage_encryption_key_plain(FogSecurityManager::CLOUD_STORAGE_SECRET_TOKEN)
    return nil if house_storage_encryption_key.nil?

    FogSecurityManager.decrypt_encrypted_string(self.encrypted_name, house_storage_encryption_key, nil, nil)# rescue nil
  end


  def address
    # => returns the decrypted plain version of the encrypted string stored in database ...

    house_storage_encryption_key = self.storage_encryption_key_plain(FogSecurityManager::CLOUD_STORAGE_SECRET_TOKEN)
    return nil if house_storage_encryption_key.nil?

    FogSecurityManager.decrypt_encrypted_string(self.encrypted_address, house_storage_encryption_key, nil, nil) rescue nil
  end


  def landmark
    # => returns the decrypted plain version of the encrypted string stored in database ...

    house_storage_encryption_key = self.storage_encryption_key_plain(FogSecurityManager::CLOUD_STORAGE_SECRET_TOKEN)
    return nil if house_storage_encryption_key.nil?

    FogSecurityManager.decrypt_encrypted_string(self.encrypted_landmark, house_storage_encryption_key, nil, nil) rescue nil
  end


  def description
    # => returns the decrypted plain version of the encrypted string stored in database ...

    house_storage_encryption_key = self.storage_encryption_key_plain(FogSecurityManager::CLOUD_STORAGE_SECRET_TOKEN)
    return nil if house_storage_encryption_key.nil?

    FogSecurityManager.decrypt_encrypted_string(self.encrypted_description, house_storage_encryption_key, nil, nil) rescue nil
  end


  def cloud_channel_key
    # => returns the decrypted plain version of the encrypted string stored in database ...

    house_storage_encryption_key = self.storage_encryption_key_plain(FogSecurityManager::CLOUD_STORAGE_SECRET_TOKEN)
    return nil if house_storage_encryption_key.nil?

    FogSecurityManager.decrypt_encrypted_string(self.encrypted_cloud_channel_key, house_storage_encryption_key, nil, nil) rescue nil
  end


  def infra_reading_computed_info_list(period = nil)
    computed_info_list = []

    period = InfraReading::INFRA_READING_PERIOD if period.nil?

    self.infra_parameters.each do | infra_parameter |
      computed_measurement_value = infra_parameter.computed_measurement([self], period)
      
      if computed_measurement_value.present?
        computed_info_list << "#{infra_parameter.name} (#{infra_parameter.retrieve_type}) - #{computed_measurement_value} #{infra_parameter.unit}"
      end
    end

    computed_info_list
  end

end