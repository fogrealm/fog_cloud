module Attributes::InfraParameter

  def computed_measurement(houses_scoped = nil, period = nil)
    period = InfraReading::INFRA_READING_PERIOD 
    houses_scoped = houses_scoped || House.all
    houses_scoped_ids = houses_scoped.map(&:id)

    end_time = Time.now
    start_time = end_time - period

    infra_readings = self.infra_readings.where(house_id: houses_scoped_ids).created_between(start_time, end_time)
    infra_reading_values = infra_readings.map(&:value)

    computed_measurement_value = nil
    if self.retrieve_type == InfraParameter::INFRA_PARAMETER_RETRIEVE_TYPE_SUM
      computed_measurement_value = infra_reading_values.sum
    elsif self.retrieve_type == InfraParameter::INFRA_PARAMETER_RETRIEVE_TYPE_AVERAGE
      computed_measurement_value = infra_reading_values.average
    elsif self.retrieve_type == InfraParameter::INFRA_PARAMETER_RETRIEVE_TYPE_LAST_VALUE
      computed_measurement_value = infra_reading_values.last
    end
      
    computed_measurement_value
  end
end