ActiveAdmin.register House do
# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
# permit_params :list, :of, :attributes, :on, :model
#
# or
#
# permit_params do
#   permitted = [:permitted, :attributes]
#   permitted << :other if params[:action] == 'create' && current_user.admin?
#   permitted
# end

  permit_params :name, :address, :landmark, :description,
                location_attributes: [ :id, :latitude, :longitude ]

  actions :show, :index

  index do
    selectable_column
    id_column
    column :name
    column :address
    column :landmark
    column :location do |house|
      "#{house.try(:location).try(:latitude)} , #{house.try(:location).try(:longitude)} "
    end
  end

  show do |house|
    attributes_table do
      row :name
      row :address
      row :landmark
      row :description
    end

    panel 'Location' do
      attributes_table_for house.location do
        row :longitude
        row :latitude
      end
    end

    panel 'Computed Readings' do
      attributes_table do
        row "Computer Infra Readings - Past #{InfraReading::INFRA_READING_PERIOD} seconds" do |house|
          bullet_list(house.infra_reading_computed_info_list(InfraReading::INFRA_READING_PERIOD), nil)
        end
      end
    end
  end

  controller do
    
    def scoped_collection
      # => This is only to show the house related to the admin user ...
      
      House.where(id: current_admin_user.house_id)
    end

  end

end