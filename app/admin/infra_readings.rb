ActiveAdmin.register InfraReading do
# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
# permit_params :list, :of, :attributes, :on, :model
#
# or
#
# permit_params do
#   permitted = [:permitted, :attributes]
#   permitted << :other if params[:action] == 'create' && current_user.admin?
#   permitted
# end
  
  permit_params :id, :house_id, :infra_parameter_id, :value

  actions :show, :index

  filter :house
  filter :infra_parameter
  filter :created_at

  index do
    selectable_column
    id_column
    column :house
    column :infra_parameter
    column :value

    actions
  end

  show do |infra_parameter|
    attributes_table do
      row :house
      row :infra_parameter
      row :value
    end
  end

end
