ActiveAdmin.register InfraParameter do
# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
# permit_params :list, :of, :attributes, :on, :model
#
# or
#
# permit_params do
#   permitted = [:permitted, :attributes]
#   permitted << :other if params[:action] == 'create' && current_user.admin?
#   permitted
# end

  permit_params :name, :unit, :retrieve_type
  
  actions :show, :index

  filter :name
  filter :unit

  index do
    selectable_column
    id_column
    column :name
    column :unit

    actions
  end

  show do |infra_parameter|
    attributes_table do
      row :name
      row :unit
      row :retrieve_type
    end
  end
end
