ActiveAdmin.register_page "Dashboard" do

  menu priority: 1, label: proc{ I18n.t("active_admin.dashboard") }

  content title: proc{ I18n.t("active_admin.dashboard") } do
    div class: "blank_slate_container", id: "dashboard_default_message" do
    end
    
    current_latitude = params[:current_latitude]
    current_longitude = params[:current_longitude]
    current_radius = params[:current_radius]

    columns do
      columns do
        panel "Enter Current Location" do
          form action: "/admin/dashboard", method: "get" do
            span class: "blank_slate" do
              "Current Latitude: "
            end
            input type: "number", name: "current_latitude", step: "any", value: current_latitude, required: true
            br
            

            span class: "blank_slate" do
              "Current Longitude: "
            end
            input type: "number", name: "current_longitude", step: "any", value: current_longitude, required: true
            br


            span class: "blank_slate" do
              "Radius (km): "
            end
            input type: "number", name: "current_radius", step: "any", value: current_radius, required: true 


            br br

            input type: "submit", value: "Fetch Details"
          end
        end
      end
    end    


    if current_longitude.present? && current_longitude.present? && current_radius.present?
      
      nearby_houses_locations = Location.locatable_type_house.near([current_latitude, current_longitude], current_radius)
      nearby_houses_location_ids = nearby_houses_locations.map(&:locatable_id)
      nearby_houses = House.where(id: nearby_houses_location_ids)

      house_events = HouseEvent.where(house_id: nearby_houses_location_ids)

      columns do
        columns do
          panel "Current Location Details" do
            ul do
              span style: "border-bottom: 1px solid black; " do 
                b do
                  "Location Details"
                end
              end
              li do
                "Latitude - #{current_latitude}"
              end
              li do 
                "Longitude - #{current_longitude}"
              end
              li do  
                "Radius - #{current_radius}"
              end
            end

            ul do
              span style: "border-bottom: 1px solid black; " do 
                b do
                  "House Events Captured Details"
                end
              end
              li do
                "No of House Events - #{house_events.count}"
              end
            end
            
            ul do
              span style: "border-bottom: 1px solid black; " do 
                b do
                  "Infra Parameter Details - (Based on Reading from End Devices installed on Houses) - (Last #{InfraReading::INFRA_READING_PERIOD}) seconds"
                end
              end
              InfraParameter.all.each do |infra_parameter|
                li do
                  computed_measurement = infra_parameter.computed_measurement(nearby_houses, InfraReading::INFRA_READING_PERIOD)
                  "#{infra_parameter.name} (#{infra_parameter.retrieve_type})  -  #{computed_measurement}"
                end
              end
            end
          end
        end
      end
    end

    # Here is an example of a simple dashboard with columns and panels.
    #
    # columns do
    #   column do
    #     panel "Recent Posts" do
    #       ul do
    #         Post.recent(5).map do |post|
    #           li link_to(post.title, admin_post_path(post))
    #         end
    #       end
    #     end
    #   end

    #   column do
    #     panel "Info" do
    #       para "Welcome to ActiveAdmin."
    #     end
    #   end
    # end
  end # content
end
