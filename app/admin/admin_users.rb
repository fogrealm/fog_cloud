ActiveAdmin.register AdminUser do
  permit_params :email, :name, :password, :password_confirmation, :phone_number, :house_id

  actions :show, :index

  index do
    selectable_column
    id_column
    column :email
    column :name
    column :phone_number
    column :house
  
    actions
  end

  filter :email
  filter :house
  
  show do |admin_user|
    attributes_table do
      row :email
      row :name
      row :phone_number
      row :house 
    end
  end

end
