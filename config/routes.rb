Rails.application.routes.draw do
  devise_for :admin_users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)

  root to: 'admin/dashboard#index'
  
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  resource :static_pages, only: [] do
    collection do
      get :home
    end
  end

  resources :house_events do
  end

  resources :infra_readings do
  end

  resources :infra_parameters do
    collection do
      post :sync_all_infra_parameters
    end
  end

  resources :houses do
    collection do
      post :sync_all_houses
    end

    collection do
      post :analyze_performance
    end
  end
end